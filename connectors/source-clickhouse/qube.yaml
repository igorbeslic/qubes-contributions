qubeKey: source-clickhouse
name: source-clickhouse
type: airbyte-source
id: source-clickhouse
maturity: alpha
ui:
  icon: <svg xmlns="http://www.w3.org/2000/svg" width="250" height="250" fill="none"><g
    clip-path="url(#a)"><path fill="red" d="M29.01 189.5h21.33V211H29.01v-21.5Z"/><path
    fill="#FC0" d="M29.01 39h21.332v150.5H29.01V39Zm42.663 0h21.331v172h-21.33V39Zm42.663
    0h21.331v172h-21.331V39Zm42.662 0h21.331v172h-21.331V39Zm42.662 69.875h21.332v32.25H199.66v-32.25Z"/></g><defs><clipPath
    id="a"><path fill="#fff" d="M29 39h192v172H29z"/></clipPath></defs></svg>
  shortDescription: database
  description: ''
  links:
  - linkName: Documentation
    url: https://docs.airbyte.com/integrations/sources/clickhouse
properties:
  type: object
  title: ClickHouse Source Spec
  schema: http://json-schema.org/draft-07/schema#
  required:
  - host
  - port
  - database
  - username
  properties:
    ssl:
      type: boolean
      order: 6
      title: SSL Connection
      default: true
      description: Encrypt data using SSL.
    host:
      type: string
      order: 0
      title: Host
      description: The host endpoint of the Clickhouse cluster.
    port:
      type: integer
      order: 1
      title: Port
      default: 8123
      maximum: 65536
      minimum: 0
      examples:
      - '8123'
      description: The port of the database.
    database:
      type: string
      order: 2
      title: Database
      examples:
      - default
      description: The name of the database.
    password:
      type: string
      order: 4
      title: Password
      description: The password associated with this username.
      airbyte_secret: true
    username:
      type: string
      order: 3
      title: Username
      description: The username which is used to access the database.
    tunnel_method:
      type: object
      oneOf:
      - title: No Tunnel
        required:
        - tunnel_method
        properties:
          tunnel_method:
            type: string
            const: NO_TUNNEL
            order: 0
            description: No ssh tunnel needed to connect to database
      - title: SSH Key Authentication
        required:
        - tunnel_method
        - tunnel_host
        - tunnel_port
        - tunnel_user
        - ssh_key
        properties:
          ssh_key:
            type: string
            order: 4
            title: SSH Private Key
            multiline: true
            description: OS-level user account ssh key credentials in RSA PEM format
              ( created with ssh-keygen -t rsa -m PEM -f myuser_rsa )
            airbyte_secret: true
          tunnel_host:
            type: string
            order: 1
            title: SSH Tunnel Jump Server Host
            description: Hostname of the jump server host that allows inbound ssh
              tunnel.
          tunnel_port:
            type: integer
            order: 2
            title: SSH Connection Port
            default: 22
            maximum: 65536
            minimum: 0
            examples:
            - '22'
            description: Port on the proxy/jump server that accepts inbound ssh connections.
          tunnel_user:
            type: string
            order: 3
            title: SSH Login Username
            description: OS-level username for logging into the jump server host.
          tunnel_method:
            type: string
            const: SSH_KEY_AUTH
            order: 0
            description: Connect through a jump server tunnel host using username
              and ssh key
      - title: Password Authentication
        required:
        - tunnel_method
        - tunnel_host
        - tunnel_port
        - tunnel_user
        - tunnel_user_password
        properties:
          tunnel_host:
            type: string
            order: 1
            title: SSH Tunnel Jump Server Host
            description: Hostname of the jump server host that allows inbound ssh
              tunnel.
          tunnel_port:
            type: integer
            order: 2
            title: SSH Connection Port
            default: 22
            maximum: 65536
            minimum: 0
            examples:
            - '22'
            description: Port on the proxy/jump server that accepts inbound ssh connections.
          tunnel_user:
            type: string
            order: 3
            title: SSH Login Username
            description: OS-level username for logging into the jump server host
          tunnel_method:
            type: string
            const: SSH_PASSWORD_AUTH
            order: 0
            description: Connect through a jump server tunnel host using username
              and password authentication
          tunnel_user_password:
            type: string
            order: 4
            title: Password
            description: OS-level password for logging into the jump server host
            airbyte_secret: true
      title: SSH Tunnel Method
      description: Whether to initiate an SSH tunnel before connecting to the database,
        and if so, which kind of authentication to use.
    jdbc_url_params:
      type: string
      order: 5
      title: JDBC URL Parameters (Advanced)
      description: Additional properties to pass to the JDBC URL string when connecting
        to the database formatted as 'key=value' pairs separated by the symbol '&'.
        (Eg. key1=value1&key2=value2&key3=value3). For more information read about
        <a href="https://jdbc.postgresql.org/documentation/head/connect.html">JDBC
        URL parameters</a>.
schemaType: {}
