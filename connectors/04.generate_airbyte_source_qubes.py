### Step
# POPULATE the required properties
# GENERATE the `properties` section
# SAVE to corresponding qube.yaml
### Result
# ├── source-github
# │ ├── metadata.yaml
# │ ├── qube.yaml
# │ ├── source_definition.json
# │ └── source_definition_specification.json
# └── source-monday
#   ├── metadata.yaml
#   ├── qube.yaml
#   ├── source_definition.json
#   └── source_definition_specification.json

import os
from decouple import config
import json, yaml

FORMAT_INDENT = config('FORMAT_INDENT', cast=int)
CONNECTOR_DIR = config('CONNECTOR_DIR')

def replace_special_keys(data: dict, special_char="$") -> dict:
    """
    Recursively replaces any keys in a nested dictionary starting with a special character with the key without the character.

    Args:
    data: The nested dictionary to modify.
    special_char: The special character to replace (default: "$").

    Returns:
    A new nested dictionary with the modified keys.
    """
    new_dict = {}
    for key, value in data.items():
        if key.startswith(special_char):
            new_key = key[len(special_char):]
            new_dict[new_key] = value
        elif isinstance(value, dict):
            new_dict[key] = replace_special_keys(value, special_char)
        else:
            new_dict[key] = value
    return new_dict


def list_keys_with_paths(data, path=[], property_list=[]):
    '''return the properties inside source_definition_specification.json

    Args:
        data (_type_): the dict itself
        path (list, optional): for tracking the path. Defaults to [].
        property_list (list, optional): whatever key has `title` in its value
    '''    
    if isinstance(data, dict):
        for key, value in data.items():
            # cummulate path for retrive (if needed)
            current_path = path + [key]
            
            if isinstance(value, dict): 
                # extract any dict that is a property
                if 'title' in value.keys():
                    property_list.append({key: value})
                list_keys_with_paths(value, current_path, property_list)
            elif isinstance(value, list):
                list_keys_with_paths(value, current_path, property_list)
            # else:
            #     print(f'{value} - {current_path}')
    elif isinstance(data, list):
        for index, item in enumerate(data):
            if isinstance(item, dict):
                for key, value in item.items():
                    current_path = path + [key]
                    if isinstance(value, dict): # for the key
                        if 'title' in value.keys():
                            property_list.append(key)
                        list_keys_with_paths(value, current_path, property_list)
                    elif isinstance(value, list):
                        list_keys_with_paths(value, current_path, property_list)
                    # else:
                    #     print(f'{value} - {current_path}')


def convert_property_list(property_list: list) -> dict:
    '''convert the list of properties in source_definition_specification.json 
        into qube.yaml format

    Args:
        property_list (list): This should be the list contains all the key 
        from the source_definition_specification

    Returns:
        dict: This should be the dict of `property` with qube.yaml format
    '''

    qube_property = {}
    for property_key in property_list:
        qube_property_block = {
            'type': 'text',
            'displayName': '',
            'defaultValue': '',
            'readOnly': False,
            'required': False,
            'validationRegex': '.*'
        }
        
        # populate each property
        for key, value in property_key.items():
            if 'title' in value.keys():
                qube_property_block['displayName'] = value['title']
            
            if 'type' in value.keys():
                if value['type'] == ['string', 'boolean'] :
                    qube_property_block['type'] = 'text'
                elif value['type'] in ['integer']:
                    qube_property_block['type'] = 'number'

            if 'default' in value.keys():
                qube_property_block['defaultValue'] = value['default']
                
            if 'pattern' in value.keys():
                qube_property_block['validationRegex'] = value['pattern']
            
            qube_property[key] = qube_property_block
            
    return qube_property
        

def generate_qube_property(qube_key: str) -> dict:
    '''generate qube.yaml's properties

    Args:
        qube_key (str): the qubeKey, should match with the source directory name

    Returns:
        dict: the qube.yaml's properties
    '''
    qube_directory = f'{CONNECTOR_DIR}/{qube_key}'

    property_list = []
    try:
        with open(f'{qube_directory}/source_definition_specification.json', 'r') as file:
            contents = json.load(file)
        
        if 'connectionSpecification' in contents.keys():
            property_section = contents['connectionSpecification']
        path = []
        
        list_keys_with_paths(property_section, path, property_list)

    except Exception as e:
        print(f'ERROR-{qube_key}: {e}')
    
    property_section = convert_property_list(property_list)
    return property_section

def generate_qube_property_spec(qube_key: str) -> dict:
    '''generate qube.yaml's properties

    Args:
        qube_key (str): the qubeKey, should match with the source directory name

    Returns:
        dict: the qube.yaml's properties
    '''
    qube_directory = f'{CONNECTOR_DIR}/{qube_key}'

    property_list = []
    sanitized_connectionspec = ""
    try:
        with open(f'{qube_directory}/source_definition_specification.json', 'r') as file:
            contents = json.load(file)
        
        if 'connectionSpecification' in contents.keys():
            connectionspec_section = contents['connectionSpecification']

        # remove $ in $schema
        sanitized_connectionspec = replace_special_keys(connectionspec_section, "$")
        
    except Exception as e:
        print(f'ERROR-{qube_key}: {e}')
    
    return sanitized_connectionspec

def generate_qube_maturity(qube_key: str) -> str:
    '''generate qube.yaml's maturity

    Args:
        qube_key (str): the qubeKey, should match with the source directory name

    Returns:
        str: the qube.yaml's maturity
    '''
    qube_directory = f'{CONNECTOR_DIR}/{qube_key}'
    maturity_section = ''
    try:
        with open(f'{qube_directory}/metadata.yaml', 'r') as file:
            contents = yaml.safe_load(file)

        if 'data' in contents.keys():
            if 'releaseStage' in contents['data'].keys():
                maturity_section = contents['data']['releaseStage']

    except Exception as e:
        print(f'ERROR-{qube_key}: {e}')
        
    return maturity_section


def generate_qube_ui(qube_key: str) -> dict:
    '''generate qube.yaml's ui

    Args:
        qube_key (str): the qubeKey, should match with the source directory name

    Returns:
        dict: the qube.yaml's properties
    '''
    qube_directory = f'{CONNECTOR_DIR}/{qube_key}'
    
    ui_section = {
        'icon': '',
        'shortDescription': '',
        'description': '',
        'links': []
    }
    try:
        # fetch links, shortDescription
        with open(f'{qube_directory}/metadata.yaml', 'r') as file:
            metadata_contents = yaml.safe_load(file)
        
        
        if 'data' in metadata_contents.keys():
            if 'documentationUrl' in metadata_contents['data'].keys():
                ui_section['links'].append({
                    'linkName': 'Documentation',
                    'url': metadata_contents['data']['documentationUrl']
                    })
            if 'connectorSubtype' in metadata_contents['data'].keys():
                ui_section['shortDescription'] = metadata_contents['data']['connectorSubtype']

        #fetch icon
        with open(f'{qube_directory}/source_definition.json', 'r') as file:
            sourcedef_contents = json.load(file)
        
        if 'icon' in sourcedef_contents.keys():
            ui_section['icon'] = sourcedef_contents['icon']
        else:    
            ui_section['icon'] = f'https://connectors.airbyte.com/files/metadata/airbyte/{qube_key}/latest/icon.svg'        
            
    except Exception as e:
        print(f'ERROR-{qube_key}: {e}')
        
    return ui_section


def generate_qube_yaml() -> None:
    for qube_key in next(os.walk(CONNECTOR_DIR))[1]:
        if qube_key.startswith('source-'):
            print(f'Processing {qube_key}')
            # source_dir_name = qubeKey e.g source-github
            #properties = generate_qube_property(qube_key)
            properties = generate_qube_property_spec(qube_key)
            maturity = generate_qube_maturity(qube_key)
            ui = generate_qube_ui(qube_key)
            qube_json = {
                'qubeKey': qube_key,
                'name': qube_key,
                'type': 'airbyte-source',
                'id': qube_key,
                'maturity': maturity,
                'ui': ui,
                'properties': properties,
                'schemaType': {}
                }
            
            with open(f'{CONNECTOR_DIR}/{qube_key}/qube.yaml', 'w') as file:
                yaml.safe_dump(qube_json, file, default_flow_style=False, sort_keys=False)


if __name__ == '__main__':
    generate_qube_yaml()
