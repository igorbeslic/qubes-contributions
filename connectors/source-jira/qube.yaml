qubeKey: source-jira
name: source-jira
type: airbyte-source
id: source-jira
maturity: generally_available
ui:
  icon: <svg xmlns="http://www.w3.org/2000/svg" width="250" height="250" fill="none"><g
    clip-path="url(#a)"><path fill="#2684FF" d="m218.678 119.462-85.395-85.143-8.275-8.25-64.283
    64.093-29.395 29.3a7.838 7.838 0 0 0 0 11.076l58.728 58.555 34.95 34.838 64.275-64.093
    1.001-.99 28.394-28.266a7.818 7.818 0 0 0 0-11.12Zm-93.67 34.793L95.666 125l29.342-29.255L154.341
    125l-29.333 29.255Z"/><path fill="url(#b)" d="M125.008 95.745a49.172 49.172 0
    0 1-14.47-34.675 49.17 49.17 0 0 1 14.257-34.762L60.592 90.293l34.941 34.838 29.475-29.387Z"/><path
    fill="url(#c)" d="m154.421 124.921-29.413 29.334a49.188 49.188 0 0 1 14.467 34.829
    49.188 49.188 0 0 1-14.467 34.829l64.381-64.155-34.968-34.837Z"/></g><defs><linearGradient
    id="b" x1="119.754" x2="78.901" y1="66.145" y2="107.11" gradientUnits="userSpaceOnUse"><stop
    offset=".18" stop-color="#0052CC"/><stop offset="1" stop-color="#2684FF"/></linearGradient><linearGradient
    id="c" x1="7506.27" x2="11682.8" y1="8615.53" y2="11331.2" gradientUnits="userSpaceOnUse"><stop
    offset=".18" stop-color="#0052CC"/><stop offset="1" stop-color="#2684FF"/></linearGradient><clipPath
    id="a"><path fill="#fff" d="M29 26h192v198H29z"/></clipPath></defs></svg>
  shortDescription: api
  description: ''
  links:
  - linkName: Documentation
    url: https://docs.airbyte.com/integrations/sources/jira
properties:
  type: object
  title: Jira Spec
  schema: http://json-schema.org/draft-07/schema#
  required:
  - api_token
  - domain
  - email
  properties:
    email:
      type: string
      order: 2
      title: Email
      description: The user email for your Jira account which you used to generate
        the API token. This field is used for Authorization to your account by BasicAuth.
    domain:
      type: string
      order: 1
      title: Domain
      examples:
      - <your-domain>.atlassian.net
      - <your-domain>.jira.com
      - jira.<your-domain>.com
      description: The Domain for your Jira account, e.g. airbyteio.atlassian.net,
        airbyteio.jira.com, jira.your-domain.com
    projects:
      type: array
      items:
        type: string
      order: 3
      title: Projects
      examples:
      - PROJ1
      - PROJ2
      description: List of Jira project keys to replicate data for, or leave it empty
        if you want to replicate data for all projects.
    api_token:
      type: string
      order: 0
      title: API Token
      description: Jira API Token. See the <a href="https://docs.airbyte.com/integrations/sources/jira">docs</a>
        for more information on how to generate this key. API Token is used for Authorization
        to your account by BasicAuth.
      airbyte_secret: true
    start_date:
      type: string
      order: 4
      title: Start Date
      format: date-time
      pattern: ^[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}Z$
      examples:
      - '2021-03-01T00:00:00Z'
      description: The date from which you want to replicate data from Jira, use the
        format YYYY-MM-DDT00:00:00Z. Note that this field only applies to certain
        streams, and only data generated on or after the start date will be replicated.
        Or leave it empty if you want to replicate all data. For more information,
        refer to the <a href="https://docs.airbyte.com/integrations/sources/jira/">documentation</a>.
    render_fields:
      type: boolean
      title: Render Issue Fields
      default: false
      description: (DEPRECATED) Render issue fields in HTML format in addition to
        Jira JSON-like format.
      airbyte_hidden: true
    expand_issue_changelog:
      type: boolean
      title: Expand Issue Changelog
      default: false
      description: (DEPRECATED) Expand the changelog when replicating issues.
      airbyte_hidden: true
    expand_issue_transition:
      type: boolean
      title: Expand Issue Transitions
      default: false
      description: (DEPRECATED) Expand the transitions when replicating issues.
      airbyte_hidden: true
    issues_stream_expand_with:
      type: array
      items:
        enum:
        - renderedFields
        - transitions
        - changelog
        type: string
      title: Expand Issues stream
      default: []
      description: 'Select fields to Expand the `Issues` stream when replicating with: '
      airbyte_hidden: true
    enable_experimental_streams:
      type: boolean
      order: 5
      title: Enable Experimental Streams
      default: false
      description: Allow the use of experimental streams which rely on undocumented
        Jira API endpoints. See https://docs.airbyte.com/integrations/sources/jira#experimental-tables
        for more info.
  additionalProperties: true
schemaType: {}
