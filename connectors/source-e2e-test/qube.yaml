qubeKey: source-e2e-test
name: source-e2e-test
type: airbyte-source
id: source-e2e-test
maturity: alpha
ui:
  icon: <svg xmlns="http://www.w3.org/2000/svg" width="250" height="250" fill="none"><path
    fill="#615EFF" fill-rule="evenodd" d="M95.775 53.416c20.364-22.88 54.087-29.592
    81.811-16.385 36.836 17.549 50.274 62.252 30.219 96.734l-45.115 77.487a18.994
    18.994 0 0 1-11.536 8.784 19.12 19.12 0 0 1-14.412-1.878l54.62-93.829c14.55-25.027
    4.818-57.467-21.888-70.24-20.038-9.583-44.533-4.795-59.336 11.685a50.008 50.008
    0 0 0-12.902 32.877 49.989 49.989 0 0 0 16.664 37.87l-31.887 54.875a18.917 18.917
    0 0 1-4.885 5.534 19.041 19.041 0 0 1-6.647 3.255 19.13 19.13 0 0 1-7.395.482
    19.087 19.087 0 0 1-7.018-2.365l34.617-59.575a68.424 68.424 0 0 1-10.524-23.544l-21.213
    36.579a18.994 18.994 0 0 1-11.535 8.784A19.123 19.123 0 0 1 33 158.668l54.856-94.356a70.296
    70.296 0 0 1 7.919-10.896Zm63.314 30.034c13.211 7.577 17.774 24.427 10.13 37.54l-52.603
    90.251a18.997 18.997 0 0 1-11.536 8.784 19.122 19.122 0 0 1-14.412-1.878l48.843-84.024a27.778
    27.778 0 0 1-10.825-4.847 27.545 27.545 0 0 1-7.783-8.907 27.344 27.344 0 0 1-3.307-11.326
    27.293 27.293 0 0 1 1.776-11.66 27.454 27.454 0 0 1 6.533-9.846 27.703 27.703
    0 0 1 10.087-6.222 27.858 27.858 0 0 1 23.097 2.135Zm-19.134 16.961a8.645 8.645
    0 0 0-2.232 2.529h-.003a8.565 8.565 0 0 0 .632 9.556 8.68 8.68 0 0 0 4.097 2.915
    8.738 8.738 0 0 0 5.036.163 8.692 8.692 0 0 0 4.279-2.642 8.59 8.59 0 0 0 2.079-4.558
    8.563 8.563 0 0 0-.821-4.938 8.645 8.645 0 0 0-3.444-3.652 8.72 8.72 0 0 0-6.586-.86
    8.7 8.7 0 0 0-3.037 1.487Z" clip-rule="evenodd"/></svg>
  shortDescription: api
  description: ''
  links:
  - linkName: Documentation
    url: https://docs.airbyte.com/integrations/sources/e2e-test
properties:
  type: object
  oneOf:
  - type: object
    title: Legacy Infinite Feed
    required:
    - type
    - max_records
    properties:
      type:
        type: string
        const: INFINITE_FEED
        default: INFINITE_FEED
      max_records:
        type: integer
        title: Max Records
        description: Number of records to emit. If not set, defaults to infinity.
      message_interval:
        type: integer
        title: Message Interval
        description: Interval between messages in ms.
    description: This mode is used for Platform acceptance tests. The catalog has
      one "data" stream, which has one string field "column1". This mode will emit
      messages infinitely.
    additionalProperties: true
  - type: object
    title: Legacy Exception After N
    required:
    - type
    - throw_after_n_records
    properties:
      type:
        type: string
        const: EXCEPTION_AFTER_N
        default: EXCEPTION_AFTER_N
      throw_after_n_records:
        min: 1
        type: integer
        title: Throw After N Records
        description: Number of records to emit before throwing an exception. Min 1.
    description: This mode is used for Platform acceptance tests. The catalog has
      one "data" stream, which has one string field "column1". This mode will throw
      an exception after N messages.
    additionalProperties: true
  - type: object
    title: Continuous Feed
    required:
    - type
    - max_messages
    - mock_catalog
    properties:
      seed:
        max: 1000000
        min: 0
        type: integer
        order: 30
        title: Random Seed
        default: 0
        examples:
        - 42
        description: 'When the seed is unspecified, the current time millis will be
          used as the seed. Range: [0, 1000000].'
      type:
        type: string
        const: CONTINUOUS_FEED
        order: 10
        default: CONTINUOUS_FEED
      max_messages:
        max: 100000000000
        min: 1
        type: integer
        order: 20
        title: Max Records
        default: 100
        description: Number of records to emit per stream. Min 1. Max 100 billion.
      mock_catalog:
        type: object
        oneOf:
        - type: object
          title: Single Schema
          required:
          - type
          - stream_name
          - stream_schema
          properties:
            type:
              type: string
              const: SINGLE_STREAM
              default: SINGLE_STREAM
            stream_name:
              type: string
              title: Stream Name
              default: data_stream
              description: Name of the data stream.
            stream_schema:
              type: string
              title: Stream Schema
              default: '{ "type": "object", "properties": { "column1": { "type": "string"
                } } }'
              description: A Json schema for the stream. The schema should be compatible
                with <a href="https://json-schema.org/draft-07/json-schema-release-notes.html">draft-07</a>.
                See <a href="https://cswr.github.io/JsonSchema/spec/introduction/">this
                doc</a> for examples.
            stream_duplication:
              max: 10000
              min: 1
              type: integer
              title: Duplicate the stream N times
              default: 1
              description: Duplicate the stream for easy load testing. Each stream
                name will have a number suffix. For example, if the stream name is
                "ds", the duplicated streams will be "ds_0", "ds_1", etc.
          description: A catalog with one or multiple streams that share the same
            schema.
        - type: object
          title: Multi Schema
          required:
          - type
          - stream_schemas
          properties:
            type:
              type: string
              const: MULTI_STREAM
              default: MULTI_STREAM
            stream_schemas:
              type: string
              title: Streams and Schemas
              default: '{ "stream1": { "type": "object", "properties": { "field1":
                { "type": "string" } } }, "stream2": { "type": "object", "properties":
                { "field1": { "type": "boolean" } } } }'
              description: A Json object specifying multiple data streams and their
                schemas. Each key in this object is one stream name. Each value is
                the schema for that stream. The schema should be compatible with <a
                href="https://json-schema.org/draft-07/json-schema-release-notes.html">draft-07</a>.
                See <a href="https://cswr.github.io/JsonSchema/spec/introduction/">this
                doc</a> for examples.
          description: A catalog with multiple data streams, each with a different
            schema.
        order: 50
        title: Mock Catalog
      message_interval_ms:
        max: 60000
        min: 0
        type: integer
        order: 40
        title: Message Interval (ms)
        default: 0
        description: Interval between messages in ms. Min 0 ms. Max 60000 ms (1 minute).
    additionalProperties: true
  - type: object
    title: Benchmark
    required:
    - type
    - schema
    - terminationCondition
    properties:
      type:
        type: string
        const: BENCHMARK
        default: BENCHMARK
      schema:
        enum:
        - FIVE_STRING_COLUMNS
        type: string
        title: Schema
        description: schema of the data in the benchmark.
      terminationCondition:
        type: object
        oneOf:
        - type: object
          title: max records
          required:
          - type
          - max
          properties:
            max:
              type: number
            type:
              type: string
              const: MAX_RECORDS
              default: MAX_RECORDS
        title: Termination Condition
        description: when does the benchmark stop?
    description: This mode is used for speed benchmarks. Specifically, it should be
      used for testing the throughput of the platform and destination. It optimizes
      for emitting records very quickly, so that it should never be the bottleneck.
    additionalProperties: true
  title: E2E Test Source Spec
  schema: http://json-schema.org/draft-07/schema#
  additionalProperties: true
schemaType: {}
